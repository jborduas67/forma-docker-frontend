# NGiNX
FROM nginx:1.15

EXPOSE 80

COPY nginx.conf /etc/nginx/nginx.conf
COPY html /usr/share/nginx/html